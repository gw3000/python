class Employee():
    def __init__(self, first_name, last_name, pay):
        self.first_name = first_name
        self.last_name = last_name
        self.email = f'{self.first_name}.{self.last_name}@email.com'
        self.pay = pay

    def fullname(self):
        print(f'The Employees name: {self.first_name} {self.last_name}.')
