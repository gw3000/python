def m():
    print('call static method')

class Konto():
    """Attempt to describe a Konto"""
    m = staticmethod(m)
    def __init__(self, inhaber, kontonummer, kontostand, max_tagesumsatz=1500):
        self.Inhaber = inhaber
        self.Kontonummer = kontonummer
        self.Kontostand = kontostand
        self.MaxTagesumsatz = max_tagesumsatz
        self.UmsatzHeute = 0

    def geldtransfer(self, ziel, betrag):
        """Geldtransfer Methode"""
        if (betrag < 0 or self.UmsatzHeute+betrag > self.MaxTagesumsatz or ziel.UmsatzHeute + betrag > ziel.MaxTagesumsatz):
            return False
        else:
            self.Kontostand -= betrag
            self.MaxTagesumsatz += betrag
            ziel.Kontostand += betrag
            ziel.UmsatzHeute += betrag
            return True

    def einzahlen(self, betrag):
        if betrag < 0 or self.UmsatzHeute+betrag > self.MaxTagesumsatz:
            return False
        else:
            self.Kontostand += betrag
            self.MaxTagesumsatz += betrag
            return True

    def auszahlen(self, betrag):
        if betrag < 0 or self.UmsatzHeute+betrag > self.MaxTagesumsatz:
            return False
        else:
            self.Kontostand -= betrag
            self.MaxTagesumsatz += betrag

    def zeige(self):
        print(f'Konto von {self.Inhaber}:')
        print(f'Aktueller Konotostand: {self.Kontostand:.2f} Euro')
        print(f'Heute wurden schon {self.UmsatzHeute:.2f} Euro von {self.MaxTagesumsatz:.2f} Euro umgesetzt!\n')


k1 = Konto('Gunther', 'A123998', 12350.00)
k2 = Konto('Conny', "A456665", 15000.00)


k1.geldtransfer(k2, 160)
k2.geldtransfer(k1, 1000)
k2.geldtransfer(k1, 500)
k2.einzahlen(500)

k1.zeige()
k2.zeige()

k1.m()
Konto.m()