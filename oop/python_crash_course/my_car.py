from car import Car
from electric_car import ElectricCar

my_car = Car('Mercedes Benz', 'E 220D', '2017')
my_car.fill_gas_tank()
print(my_car.get_descriptive_name())

# my_car.odometre = 120
# my_car.read_odometre()

# my_car.update_odometre(10000)
# my_car.update_odometre(400)
# my_car.read_odometre()

# my_car.read_year()
# my_car.change_year(2016)
# my_car.read_year()
# my_car.increment_odometre(-100)
# my_car.read_odometre()

my_tesla = ElectricCar('tesla', 'model s', 2018)
# my_tesla.fill_gas_tank()
my_tesla.battery.battery_size = 70
my_tesla.battery.describe_battery()
my_tesla.battery.get_range()
