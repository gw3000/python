from car import Car


class Battery():
    """A simple attempt to model a battery for an electric car"""

    def __init__(self, battery_size=70):
        """Init the battery's attribute"""
        self.battery_size = battery_size

    def describe_battery(self):
        """Print a statement decribing the battery size"""
        print(f'This car has a {self.battery_size}-kWh battery.')

    def get_range(self):
        """Print a statement about the range this battery provides."""
        if self.battery_size == 70:
            range = 240
        elif self.battery_size == 85:
            range = 270

        print(
            f'This car can go approximatly {str(range)} miles on a full charge')


class ElectricCar(Car):
    """Represents aspects of a car, specific to electric ones"""

    def __init__(self, make, model, year):
        """Init attributes of the parent class.
        Then init attributes specific to an electric car."""
        super().__init__(make, model, year)
        self.battery = Battery()

    def fill_gas_tank(self):
        """Of course you do not need a gas tank"""
        print(f'You do not have any gas tank!')
