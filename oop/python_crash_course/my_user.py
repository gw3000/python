from user import User
from admin import Administrator

gw = User('Gunther', 'Weissenbaeck', '2977-12-04', 'm')
gw.increment_login_attempts()
gw.increment_login_attempts()
gw.increment_login_attempts()
gw.increment_login_attempts()
print(gw.login_attempts)
gw.reset_login_attempts()
print(gw.login_attempts)

admin_1 = Administrator('Gunther', 'Weissenbaeck', '1977-12-04',
                        'M', 0)
admin_1.privileges.privileges = ['can add post', 'can delete post']
admin_1.privileges.show_privileges()
# admin_1.privileges.show_privileges()
