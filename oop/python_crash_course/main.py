from user import *

user_1 = User('Charlotte', 'Weissenbaeck', '23.02.2014', 'w')
user_1.describe_user()
user_1.greet_user()

print("\n")

user_2 = User('Gunther', 'Weissenbaeck', '04.12.1977', 'm')
# print(user_2.dob)
user_2.describe_user()
user_2.greet_user()

print("\n")

user_3 = User('Clara', 'Weißenbäck', '29.12.2009', 'w')
user_3.describe_user()
user_3.greet_user()
