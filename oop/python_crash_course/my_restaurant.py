from restaurant import Restaurant, IceCreamStand

# myRestaurant = Restaurant('Knolle', 'Klöse')

# print(myRestaurant.restaurant_name)
# print(myRestaurant.cuisine_type)
# myRestaurant.describe_restaurant()
# myRestaurant.open_restaurant()

# myRestaurant.show_served()
# myRestaurant.increment_number_served(1)
# myRestaurant.show_served()


myIceCreamStand = IceCreamStand('Luigis Stand', 'Ice Creams', ['strawberry', 'banana', 'blueberry'])
myIceCreamStand.list_ice_cream_flavors()