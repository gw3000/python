class Privileges():
    """A simple Privileges class"""

    def __init__(self, privileges=[]):
        self.privileges = privileges

    def show_privileges(self):
        """Show admin previlges"""
        for item in self.privileges:
            print(item)
