class User:
    def __init__(self, first_name, last_name, dob, gender, login_attempts=0):
        """A simple class for a user"""
        self.first_name = first_name
        self.last_name = last_name
        self.dob = dob
        self.gender = gender
        self.login_attempts = login_attempts

    def describe_user(self):
        """A simple method to describe a user"""
        fullname = self.first_name + " " + self.last_name
        print("The user's name is {}.".format(fullname))
        print("The user was born in {}.".format(self.dob))
        if self.gender == 'w':
            girl_or_boy = "Girl"
        elif self.gender == 'm':
            girl_or_boy = 'Boy'
        print("The user is a {}.".format(girl_or_boy))

    def greet_user(self):
        """A simple method to greet the user"""
        if self.gender == 'w':
            print('Dear Mrs. {},'.format(self.last_name))
        else:
            print("Dear Mr. {},".format(self.last_name))

    def increment_login_attempts(self):
        """Increments the login attempts by one"""
        self.login_attempts += 1

    def reset_login_attempts(self):
        """Resets the login attempts to 0"""
        self.login_attempts = 0





