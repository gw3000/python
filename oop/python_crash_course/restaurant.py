class Restaurant:
    """A simple restaurant class"""

    def __init__(self, restaurant_name, cuisine_type, number_served=0):
        self.restaurant_name = restaurant_name
        self.cuisine_type = cuisine_type
        self.number_served = number_served

    def describe_restaurant(self):
        """describes the restaurant with its name and its cuisine"""
        print(
            f'The Restaurant {self.restaurant_name} serves {self.cuisine_type}.')

    def open_restaurant(self):
        """information if the restaurant is open"""
        print(f'The Restaurant {self.restaurant_name} has open.')

    def show_served(self):
        """Print the number of customers the restaurant has served"""
        print(f'{self.number_served} customers have been served.')

    def increment_number_served(self, number):
        """Increment the number customers have been served"""
        if int(number) > 0:
            self.number_served += int(number)
        else:
            print(f'Cannot be less then 0!')


class IceCreamStand(Restaurant):
    """A class the inherits from the restaurant class"""

    def __init__(self, restaurant_name, cuisine_type, ice_cream_flavors=[]):
        """init from parent class"""
        super().__init__(restaurant_name, cuisine_type)
        self.ice_cream_flavors = ice_cream_flavors

    def list_ice_cream_flavors(self):
        """A method to list all flavors per ice cream stand"""
        for item in self.ice_cream_flavors:
            print(item)

