from user import User
from privileges import Privileges

class Administrator(User):
    """Admin Class"""

    def __init__(self, first_name, last_name, dob, gender, login_attempts):
        """First init variables from parent class"""
        super().__init__(first_name, last_name, dob, gender, login_attempts)
        self.privileges = Privileges()

