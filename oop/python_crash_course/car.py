"""A classs that can be used to represent a car"""


class Car:
    """ A simple Class to describe a car."""

    def __init__(self, make, model, year, odometre=0):
        """Initialize attributes to describe a car"""
        self.make = make
        self.model = model
        self.year = year
        self.odometre = odometre

    def get_descriptive_name(self):
        """Return a neatly formatted descriptive name."""
        long_name = str(self.year) + ' ' + str(self.make) + \
            ' ' + str(self.model)
        return long_name.title()

    def update_odometre(self, mileage):
        """Set the odometre reading the given value."""
        if mileage >= self.odometre:
            self.odometre = mileage
        else:
            print(
                f'You cannot turn back the milage of your car ({self.make} {self.model})!')

    def increment_odometre(self, miles):
        """Increments the odometre by mileage"""
        if int(miles) > 0:
            self.odometre += miles
        else:
            print('You cannot turn back the odometre!')

    def read_odometre(self):
        """Print a statement showing the car's mileage"""
        print(f'This car has {self.odometre} miles on it.')

    def change_year(self, new_year):
        """Update the biulding year"""
        self.year = int(new_year)

    def read_year(self):
        """Outputs the year that the car was biult"""
        print(f'The Car was built in {self.year}.')

    def fill_gas_tank(self):
        """Prints you fuled up your car"""
        print(f'You fuled up your car!')
